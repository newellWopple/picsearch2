package com.example.picsearch2

import java.io.Serializable

class PixImage : Serializable {
    var id: Int = 0
    lateinit var largeImageURL: String
    lateinit var previewURL: String
    lateinit var tags: String
    lateinit var user: String

    constructor(id: Int, largeImageURL: String, previewURL: String,tags: String,user: String) {
        this.id = id
        this.largeImageURL = largeImageURL
        this.previewURL = previewURL
        this.tags = tags
        this.user = user
    }
    constructor()

}