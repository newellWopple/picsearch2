package com.example.picsearch2

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import android.widget.SearchView

import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException
import java.net.URLEncoder
import android.content.SharedPreferences
import android.os.PersistableBundle
import java.io.Serializable


const val EXTRA_PIX = "com.example.picsearch2.PIX"


class MainActivity : AppCompatActivity() {
    private lateinit var listView : ListView
    private val client = OkHttpClient()
    var arrayList:ArrayList<PixImage> = ArrayList();
    private lateinit var searchView: SearchView
    private lateinit var searchMenuItem: MenuItem
    private var lastQuery = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        listView = findViewById<ListView>(R.id.listView) as ListView
        listView.setOnItemClickListener { adaptorView, parent, position, id ->
            val pixImage = listView.adapter.getItem(position)
            val intent = Intent(this, DetailActivity::class.java).apply {
                putExtra(EXTRA_PIX, pixImage as Serializable)
            }
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()

        //doDummySearch()

        //sharedPref = getSharedPreferences("lastQuery", MODE_PRIVATE);
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchMenuItem = menu.findItem(R.id.app_bar_search)
        searchView = searchMenuItem.getActionView() as SearchView

        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setSubmitButtonEnabled(true)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                if (query != null) {
                    doPixSearch(query)
                }
                searchView.clearFocus()
                return false
            }

        })

        return true
    }



    fun handlePixData(json: String) {
        //Log.d("json", json)

        val jsonObj : JSONObject = JSONObject(json)
        var jsonHits : JSONArray = jsonObj.getJSONArray("hits")
        //Log.d("jsonHits", jsonHits.toString())


        var i : Int = 0
        var size : Int = jsonHits.length()
        arrayList = ArrayList();
        for (i in 0.. size-1) {
            var json_objectdetail:JSONObject = jsonHits.getJSONObject(i)
            var pixImage:PixImage = PixImage();
            pixImage.id=json_objectdetail.getInt("id")
            pixImage.largeImageURL=json_objectdetail.getString("largeImageURL")
            pixImage.previewURL=json_objectdetail.getString("previewURL")
            pixImage.user=json_objectdetail.getString("user")
            pixImage.tags=json_objectdetail.getString("tags")
            arrayList.add(pixImage)
        }

        val adapter : PixListAdapter
        adapter = PixListAdapter(applicationContext, arrayList)
        listView.adapter = adapter


    }


    fun doPixSearch(query : String) {

        var url = "https://pixabay.com/api/?key=12729488-f72c96e2c88b3363588ef858e&q=" + URLEncoder.encode(
            query,
            "utf-8"
        ) + "&image_type=photo"
        Log.d("url", url)

        val request = Request.Builder().url(url).build()


        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {Log.e("requestError", e.message)}
            override fun onResponse(call: Call, response: Response) {
                val resString = response.body!!.string()
                runOnUiThread { handlePixData(resString) }
                response.close()
            }
        })
    }

}
