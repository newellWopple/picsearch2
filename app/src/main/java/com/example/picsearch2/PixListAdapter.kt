package com.example.picsearch2

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class PixListAdapter(private val context: Context,
                     private val dataSource: ArrayList<PixImage>
) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var rowView = convertView
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.list_item_pix, parent, false)
        }

        val pix = getItem(position) as PixImage

        val thumbImageView = rowView!!.findViewById(R.id.thumbImageView) as ImageView
        val nameTextView = rowView!!.findViewById(R.id.nameTextView) as TextView
        val tagTextView = rowView!!.findViewById(R.id.tagTextView) as TextView

        Picasso.get().load(pix.previewURL).placeholder(R.drawable.ic_placeholder).into(thumbImageView)
        //Picasso.get().load(pix.previewURL).into(thumbImageView)
        nameTextView.text = pix.user
        tagTextView.text = pix.tags



        return rowView
    }
}