package com.example.picsearch2

import android.os.Handler
import android.util.Log
import okhttp3.*
import org.json.JSONObject
import java.io.IOException
import java.net.URLEncoder






class PixNetworkSearch {
    private val client = OkHttpClient()

    fun doPixSearch(query : String) {

        var url = "https://pixabay.com/api/?key=12729488-f72c96e2c88b3363588ef858e&q=" + URLEncoder.encode(
            query,
            "utf-8"
        ) + "&image_type=photo"
        Log.d("url", url)

        val request = Request.Builder().url(url).build()

//        val response = client.newCall(request).execute()
//        return response


        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {Log.e("requestError", e.message)}
            override fun onResponse(call: Call, response: Response) {

                response.close()
            }
        })
    }


}