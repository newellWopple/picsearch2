package com.example.picsearch2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class DetailActivity : AppCompatActivity() {
    private lateinit var pixImage : PixImage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        pixImage = intent.extras?.get(EXTRA_PIX) as PixImage

        val imageView = findViewById<ImageView>(R.id.imageView) as ImageView
        val nameTextView = findViewById<TextView>(R.id.nameTextView) as TextView
        val tagsTextView = findViewById<TextView>(R.id.tagsTextView) as TextView
        val idTextView = findViewById<TextView>(R.id.idTextView) as TextView

        nameTextView.text = pixImage.user
        tagsTextView.text = pixImage.tags
        idTextView.text = pixImage.id.toString()
        Picasso.get().load(pixImage.largeImageURL).placeholder(R.drawable.ic_placeholder).into(imageView)
    }
}
